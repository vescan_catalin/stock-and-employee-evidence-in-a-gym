package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import model.InformatiiClienti;
import model.InformatiiUtilizator;
import model.Stoc;

import javax.swing.JButton;

public class ViewInformations extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTable table;
	private DefaultTableModel model;
	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
	private JButton ok;

	public ViewInformations() {
		setTitle("Vizualizare informatii");
		setSize(1800, 800);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblInformatii = new JLabel("Informatii");
		lblInformatii.setFont(new Font("Tahoma", Font.PLAIN, 35));
		lblInformatii.setBounds(830, 25, 160, 40);
		panel.add(lblInformatii);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 86, 1770, 600);
		panel.add(scrollPane);

		table = new JTable();
		table.setRowHeight(25);
		table.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 15));
		table.setEnabled(false);
		DefaultTableCellRenderer alignment = new DefaultTableCellRenderer();
		alignment.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
		table.setDefaultRenderer(Object.class, alignment);
		scrollPane.setViewportView(table);

		ok = new JButton("OK");
		ok.setFont(new Font("Tahoma", Font.PLAIN, 30));
		ok.setBounds(850, 700, 100, 50);
		panel.add(ok);
	}

	public void okEvent(ActionListener listener) {
		ok.addActionListener(listener);
	}

	@SuppressWarnings("unchecked")
	public void setTableText(List<?> list, int type) {

		if (type == 2) { // clients informations or subscriptions expired
			table.setModel(model = new DefaultTableModel(new Object[][] {},
					new String[] { "Nume", "Prenume", "Telefon", "Data nasterii", "Data incepere abonament",
							"Data expirare abonament", "Tip abonament", "Pret", "Observatii" }));
			table.setFont(new Font("Tahoma", Font.PLAIN, 20));
			table.getColumnModel().getColumn(0).setPreferredWidth(150);
			table.getColumnModel().getColumn(1).setPreferredWidth(150);
			table.getColumnModel().getColumn(2).setPreferredWidth(150);
			table.getColumnModel().getColumn(3).setPreferredWidth(150);
			table.getColumnModel().getColumn(4).setPreferredWidth(150);
			table.getColumnModel().getColumn(5).setPreferredWidth(150);
			table.getColumnModel().getColumn(6).setPreferredWidth(150);
			table.getColumnModel().getColumn(7).setPreferredWidth(150);
			table.getColumnModel().getColumn(8).setPreferredWidth(150);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

			model.setRowCount(0);

			if (list.size() != 0) {
				List<InformatiiClienti> clients = (List<InformatiiClienti>) list;
				for (InformatiiClienti client : clients) {
					Object[] o = new Object[9];
					o[0] = client.getNumeClient();
					o[1] = client.getPrenumeClient();
					o[2] = client.getTelefonClient();
					try {
						if (client.getDataNasteriiClient() != null)
							o[3] = format.format(client.getDataNasteriiClient());
						if (client.getDataCreeareAbonament() != null)
							o[4] = format.format(client.getDataCreeareAbonament());
						else
							JOptionPane.showMessageDialog(null, "Data in care incepe abonamentul este obligatorie!",
									"Eroare", JOptionPane.ERROR_MESSAGE);
						if (client.getDataExpirareAbonament() != null)
							o[5] = format.format(client.getDataExpirareAbonament());
					} catch (Exception e) {
						e.printStackTrace();
					}
					o[6] = client.getTipAbonament();
					o[7] = client.getPret();
					o[8] = client.getObservatiiClient();

					model.addRow(o);
				}
			}
		}

		if (type == 3) { // users informations
			table.setModel(model = new DefaultTableModel(new Object[][] {},
					new String[] { "Nume", "Prenume", "Telefon", "Data nasterii", "Observatii" }));
			table.setFont(new Font("Tahoma", Font.PLAIN, 20));
			table.getColumnModel().getColumn(0).setPreferredWidth(150);
			table.getColumnModel().getColumn(1).setPreferredWidth(150);
			table.getColumnModel().getColumn(2).setPreferredWidth(150);
			table.getColumnModel().getColumn(3).setPreferredWidth(150);
			table.getColumnModel().getColumn(4).setPreferredWidth(150);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

			model.setRowCount(0);

			if (list.size() != 0) {
				List<InformatiiUtilizator> users = (List<InformatiiUtilizator>) list;
				for (InformatiiUtilizator user : users) {
					Object[] o = new Object[9];
					o[0] = user.getNumeAngajat();
					o[1] = user.getPrenumeAngajat();
					o[2] = user.getTelefonAngajat();
					try {
						if (user.getDataNasterii() != null)
							o[3] = format.format(user.getDataNasterii());
					} catch (Exception e) {
						e.printStackTrace();
					}
					o[4] = user.getObservatii();

					model.addRow(o);
				}
			}
		}

		if (type == 4) { // stock informations
			table.setModel(model = new DefaultTableModel(new Object[][] {},
					new String[] { "Nume", "Cantitate", "Pret (lei)", "Data expirarii" }));
			table.setFont(new Font("Tahoma", Font.PLAIN, 20));
			table.getColumnModel().getColumn(0).setPreferredWidth(150);
			table.getColumnModel().getColumn(1).setPreferredWidth(150);
			table.getColumnModel().getColumn(2).setPreferredWidth(150);
			table.getColumnModel().getColumn(3).setPreferredWidth(150);

			model.setRowCount(0);

			if (list.size() != 0) {
				List<Stoc> stock = (List<Stoc>) list;
				stock.forEach(stoc -> {
					Object[] o = new Object[9];
					o[0] = stoc.getNumeProdus();
					o[1] = stoc.getCantitate();
					o[2] = stoc.getPret();
					try {
						o[3] = stoc.getDataExpirarii() == null ? null : format.format(stoc.getDataExpirarii());
					} catch (Exception e) {
						e.printStackTrace();
					}

					model.addRow(o);
				});
			}
		}

	}
}
