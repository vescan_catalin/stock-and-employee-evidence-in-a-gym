package view;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class UserFrame extends JFrame {
	private JTextField numeClientText;
	private JTextField prenumeClientText;
	private JTextField telefonClientText;
	private JTextField dataNasteriiClientText;
	private JTextField creeareAbonamentText;
	private JTextField expirareAbonamentText;
	private JTextField tipAbonamentText;
	private JTextField observatiiClientiText;
	private JTextField numeProdusStocText;
	private JTextField cantitateStocText;
	private JTextField pretStocText;
	private JButton btnStergereProdus;
	private JButton btnVizualizareStoc;
	private JButton btnCreeareAbonament;
	private JButton btnVizualizareDateClienti;
	private JButton btnModificareDateClient;
	private JButton btnStergereClient;
	private JButton btnVerificareAbonamente;
	private JButton btnAdaugareProdus;
	private JButton btnVanzare;
	private JTextField dataExpirariiStocText;
	private JTextField pretAbonamentText;
	private JLabel lblNumeClient;
	private JLabel lblPrenumeClient;
	private JLabel lblTelefonClient;
	private JLabel lblDataNasteriiClient;
	private JLabel lblDataCreeareAbonament;
	private JLabel lblDataExpirareAbonament;
	private JLabel lblTipAbonament;
	private JLabel lblPret_1;
	private JLabel lblObservatiiClient;
	private JTextField numeClientNou;
	private JTextField prenumeClientNou;
	private JTextField telefonClientNou;
	private JTextField dataNasteriiClientNou;
	private JTextField creeareAbonamentNou;
	private JTextField expirareAbonamentNou;
	private JTextField tipAbonamentNou;
	private JTextField pretAbonamentNou;
	private JTextField observatiiClientiNou;
	private JTextField cantitateText;
	private JTextField numeProdusText;

	public UserFrame() {
		setTitle("Angajat PACO POWER GYM");
		setSize(1300, 800);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		tabbedPane.addTab("Gestiune clienti", null, panel, null);

		JLabel numeClient = new JLabel("Nume client");
		numeClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		numeClient.setBounds(30, 100, 200, 30);
		panel.add(numeClient);

		JLabel prenumeClient = new JLabel("Prenume client");
		prenumeClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		prenumeClient.setBounds(30, 130, 200, 30);
		panel.add(prenumeClient);

		JLabel dataNasteriiClient = new JLabel("Data nasterii client");
		dataNasteriiClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataNasteriiClient.setBounds(30, 190, 250, 30);
		panel.add(dataNasteriiClient);

		JLabel telefonClient = new JLabel("Telefon client");
		telefonClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		telefonClient.setBounds(30, 160, 200, 30);
		panel.add(telefonClient);

		JLabel tipAbonament = new JLabel("Tip abonament");
		tipAbonament.setFont(new Font("Tahoma", Font.PLAIN, 25));
		tipAbonament.setBounds(30, 280, 200, 30);
		panel.add(tipAbonament);

		JLabel titlu1 = new JLabel("PACO POWER GYM");
		titlu1.setFont(new Font("Tahoma", Font.PLAIN, 50));
		titlu1.setBounds(425, 10, 450, 50);
		panel.add(titlu1);

		numeClientText = new JTextField();
		numeClientText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		numeClientText.setColumns(10);
		numeClientText.setBounds(350, 100, 400, 30);
		panel.add(numeClientText);

		prenumeClientText = new JTextField();
		prenumeClientText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		prenumeClientText.setColumns(10);
		prenumeClientText.setBounds(350, 130, 400, 30);
		panel.add(prenumeClientText);

		telefonClientText = new JTextField();
		telefonClientText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		telefonClientText.setColumns(10);
		telefonClientText.setBounds(350, 160, 400, 30);
		panel.add(telefonClientText);

		dataNasteriiClientText = new JTextField();
		dataNasteriiClientText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataNasteriiClientText.setColumns(10);
		dataNasteriiClientText.setBounds(350, 190, 400, 30);
		panel.add(dataNasteriiClientText);

		creeareAbonamentText = new JTextField();
		creeareAbonamentText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		creeareAbonamentText.setColumns(10);
		creeareAbonamentText.setBounds(350, 220, 400, 30);
		panel.add(creeareAbonamentText);

		btnCreeareAbonament = new JButton("Creeare abonament");
		btnCreeareAbonament.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnCreeareAbonament.setBounds(800, 100, 380, 50);
		panel.add(btnCreeareAbonament);

		btnVizualizareDateClienti = new JButton("Vizualizare date clienti");
		btnVizualizareDateClienti.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnVizualizareDateClienti.setBounds(800, 180, 380, 50);
		panel.add(btnVizualizareDateClienti);

		btnModificareDateClient = new JButton("Modificare date client");
		btnModificareDateClient.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnModificareDateClient.setBounds(800, 260, 380, 50);
		panel.add(btnModificareDateClient);

		btnStergereClient = new JButton("Stergere client");
		btnStergereClient.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnStergereClient.setBounds(800, 340, 380, 50);
		panel.add(btnStergereClient);

		btnVerificareAbonamente = new JButton("Verificare abonamente");
		btnVerificareAbonamente.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnVerificareAbonamente.setBounds(800, 420, 380, 50);
		panel.add(btnVerificareAbonamente);

		JLabel dataCreeare = new JLabel("Data creeare abonament");
		dataCreeare.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataCreeare.setBounds(30, 220, 300, 30);
		panel.add(dataCreeare);

		JLabel dataExpirare = new JLabel("Data expirare abonament");
		dataExpirare.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataExpirare.setBounds(30, 250, 300, 30);
		panel.add(dataExpirare);

		expirareAbonamentText = new JTextField();
		expirareAbonamentText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		expirareAbonamentText.setColumns(10);
		expirareAbonamentText.setBounds(350, 250, 400, 30);
		panel.add(expirareAbonamentText);

		tipAbonamentText = new JTextField();
		tipAbonamentText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		tipAbonamentText.setColumns(10);
		tipAbonamentText.setBounds(350, 280, 400, 30);
		panel.add(tipAbonamentText);

		JLabel observatiiClient = new JLabel("Observatii client");
		observatiiClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		observatiiClient.setBounds(30, 340, 200, 30);
		panel.add(observatiiClient);

		observatiiClientiText = new JTextField();
		observatiiClientiText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		observatiiClientiText.setColumns(10);
		observatiiClientiText.setBounds(350, 340, 400, 30);
		panel.add(observatiiClientiText);
		
		JLabel lblPret = new JLabel("Pret");
		lblPret.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPret.setBounds(30, 310, 200, 30);
		panel.add(lblPret);
		
		pretAbonamentText = new JTextField();
		pretAbonamentText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		pretAbonamentText.setColumns(10);
		pretAbonamentText.setBounds(350, 310, 400, 30);
		panel.add(pretAbonamentText);
		
		lblNumeClient = new JLabel("Nume client *");
		lblNumeClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNumeClient.setBounds(30, 430, 200, 30);
		panel.add(lblNumeClient);
		
		lblPrenumeClient = new JLabel("Prenume client *");
		lblPrenumeClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPrenumeClient.setBounds(30, 460, 200, 30);
		panel.add(lblPrenumeClient);
		
		lblTelefonClient = new JLabel("Telefon client *");
		lblTelefonClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTelefonClient.setBounds(30, 490, 200, 30);
		panel.add(lblTelefonClient);
		
		lblDataNasteriiClient = new JLabel("Data nasterii client*");
		lblDataNasteriiClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDataNasteriiClient.setBounds(30, 520, 250, 30);
		panel.add(lblDataNasteriiClient);
		
		lblDataCreeareAbonament = new JLabel("Data creeare abonament  *");
		lblDataCreeareAbonament.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDataCreeareAbonament.setBounds(30, 550, 310, 30);
		panel.add(lblDataCreeareAbonament);
		
		lblDataExpirareAbonament = new JLabel("Data expirare abonament *");
		lblDataExpirareAbonament.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDataExpirareAbonament.setBounds(30, 580, 310, 30);
		panel.add(lblDataExpirareAbonament);
		
		lblTipAbonament = new JLabel("Tip abonament *");
		lblTipAbonament.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblTipAbonament.setBounds(30, 610, 200, 30);
		panel.add(lblTipAbonament);
		
		lblPret_1 = new JLabel("Pret *");
		lblPret_1.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblPret_1.setBounds(30, 640, 200, 30);
		panel.add(lblPret_1);
		
		lblObservatiiClient = new JLabel("Observatii client *");
		lblObservatiiClient.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblObservatiiClient.setBounds(30, 670, 200, 30);
		panel.add(lblObservatiiClient);
		
		numeClientNou = new JTextField();
		numeClientNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		numeClientNou.setColumns(10);
		numeClientNou.setBounds(350, 430, 400, 30);
		panel.add(numeClientNou);
		
		prenumeClientNou = new JTextField();
		prenumeClientNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		prenumeClientNou.setColumns(10);
		prenumeClientNou.setBounds(350, 460, 400, 30);
		panel.add(prenumeClientNou);
		
		telefonClientNou = new JTextField();
		telefonClientNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		telefonClientNou.setColumns(10);
		telefonClientNou.setBounds(350, 490, 400, 30);
		panel.add(telefonClientNou);
		
		dataNasteriiClientNou = new JTextField();
		dataNasteriiClientNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataNasteriiClientNou.setColumns(10);
		dataNasteriiClientNou.setBounds(350, 520, 400, 30);
		panel.add(dataNasteriiClientNou);
		
		creeareAbonamentNou = new JTextField();
		creeareAbonamentNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		creeareAbonamentNou.setColumns(10);
		creeareAbonamentNou.setBounds(350, 550, 400, 30);
		panel.add(creeareAbonamentNou);
		
		expirareAbonamentNou = new JTextField();
		expirareAbonamentNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		expirareAbonamentNou.setColumns(10);
		expirareAbonamentNou.setBounds(350, 580, 400, 30);
		panel.add(expirareAbonamentNou);
		
		tipAbonamentNou = new JTextField();
		tipAbonamentNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		tipAbonamentNou.setColumns(10);
		tipAbonamentNou.setBounds(350, 610, 400, 30);
		panel.add(tipAbonamentNou);
		
		pretAbonamentNou = new JTextField();
		pretAbonamentNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		pretAbonamentNou.setColumns(10);
		pretAbonamentNou.setBounds(350, 640, 400, 30);
		panel.add(pretAbonamentNou);
		
		observatiiClientiNou = new JTextField();
		observatiiClientiNou.setFont(new Font("Tahoma", Font.PLAIN, 25));
		observatiiClientiNou.setColumns(10);
		observatiiClientiNou.setBounds(350, 670, 400, 30);
		panel.add(observatiiClientiNou);
		
		JLabel lblNewLabel = new JLabel("Campurile cu * se completeaza doar in cazul modificarii datelor !");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(30, 391, 1000, 30);
		panel.add(lblNewLabel);
		
		JLabel lblTelefonulEsteUnic = new JLabel("Telefonul este unic !");
		lblTelefonulEsteUnic.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblTelefonulEsteUnic.setBounds(30, 705, 1000, 30);
		panel.add(lblTelefonulEsteUnic);
		
		JLabel lblNewLabel_1 = new JLabel("(an-luna-zi)");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(245, 190, 120, 30);
		panel.add(lblNewLabel_1);
		
		JLabel label_2 = new JLabel("(an-luna-zi)");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		label_2.setBounds(245, 520, 120, 30);
		panel.add(label_2);

		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Stoc / Vanzare", null, panel_3, null);
		panel_3.setLayout(null);

		JLabel titlu3 = new JLabel("PACO POWER GYM");
		titlu3.setBounds(425, 10, 450, 50);
		titlu3.setFont(new Font("Tahoma", Font.PLAIN, 50));
		panel_3.add(titlu3);

		JLabel lblNumeProdusStoc = new JLabel("Nume produs");
		lblNumeProdusStoc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblNumeProdusStoc.setBounds(50, 100, 250, 40);
		panel_3.add(lblNumeProdusStoc);

		JLabel lblCantitateStoc = new JLabel("Cantitate");
		lblCantitateStoc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCantitateStoc.setBounds(50, 170, 250, 40);
		panel_3.add(lblCantitateStoc);

		JLabel lblPretStoc = new JLabel("Pret");
		lblPretStoc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPretStoc.setBounds(50, 240, 250, 40);
		panel_3.add(lblPretStoc);

		numeProdusStocText = new JTextField();
		numeProdusStocText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		numeProdusStocText.setBounds(350, 100, 300, 40);
		panel_3.add(numeProdusStocText);
		numeProdusStocText.setColumns(10);

		cantitateStocText = new JTextField();
		cantitateStocText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		cantitateStocText.setColumns(10);
		cantitateStocText.setBounds(350, 170, 300, 40);
		panel_3.add(cantitateStocText);

		pretStocText = new JTextField();
		pretStocText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		pretStocText.setColumns(10);
		pretStocText.setBounds(350, 240, 300, 40);
		panel_3.add(pretStocText);

		btnAdaugareProdus = new JButton("Adaugare produs");
		btnAdaugareProdus.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnAdaugareProdus.setBounds(800, 100, 300, 40);
		panel_3.add(btnAdaugareProdus);

		btnVizualizareStoc = new JButton("Vizualizare stoc");
		btnVizualizareStoc.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnVizualizareStoc.setBounds(800, 170, 300, 40);
		panel_3.add(btnVizualizareStoc);

		btnStergereProdus = new JButton("Stergere produs");
		btnStergereProdus.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnStergereProdus.setBounds(800, 240, 300, 40);
		panel_3.add(btnStergereProdus);

		JLabel lblDataExpirarii = new JLabel("Data expirarii");
		lblDataExpirarii.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblDataExpirarii.setBounds(50, 310, 250, 40);
		panel_3.add(lblDataExpirarii);

		dataExpirariiStocText = new JTextField();
		dataExpirariiStocText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		dataExpirariiStocText.setColumns(10);
		dataExpirariiStocText.setBounds(350, 310, 300, 40);
		panel_3.add(dataExpirariiStocText);
		
		JLabel label_3 = new JLabel("Nume produs");
		label_3.setFont(new Font("Tahoma", Font.PLAIN, 30));
		label_3.setBounds(50, 550, 250, 40);
		panel_3.add(label_3);
		
		JLabel label_4 = new JLabel("Cantitate");
		label_4.setFont(new Font("Tahoma", Font.PLAIN, 30));
		label_4.setBounds(50, 634, 250, 40);
		panel_3.add(label_4);
		
		cantitateText = new JTextField();
		cantitateText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		cantitateText.setColumns(10);
		cantitateText.setBounds(350, 634, 300, 40);
		panel_3.add(cantitateText);
		
		numeProdusText = new JTextField();
		numeProdusText.setFont(new Font("Tahoma", Font.PLAIN, 30));
		numeProdusText.setColumns(10);
		numeProdusText.setBounds(350, 550, 300, 40);
		panel_3.add(numeProdusText);
		
		btnVanzare = new JButton("Vanzare");
		btnVanzare.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnVanzare.setBounds(800, 550, 300, 40);
		panel_3.add(btnVanzare);
		DefaultTableCellRenderer alignment = new DefaultTableCellRenderer();
		alignment.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
	}

	// sectiunea de evenimente pe butoane
	public void EventCreeareAbonament(ActionListener listener) {
		btnCreeareAbonament.addActionListener(listener);
	}

	public void EventVizualizareDateClienti(ActionListener listener) {
		btnVizualizareDateClienti.addActionListener(listener);
	}

	public void EventModificareDateClient(ActionListener listener) {
		btnModificareDateClient.addActionListener(listener);
	}

	public void EventStergereClient(ActionListener listener) {
		btnStergereClient.addActionListener(listener);
	}

	public void EventVerificareAbonamente(ActionListener listener) {
		btnVerificareAbonamente.addActionListener(listener);
	}

	public void EventVanzare(ActionListener listener) {
		btnVanzare.addActionListener(listener);
	}

	public void EventAdaugareProdus(ActionListener listener) {
		btnAdaugareProdus.addActionListener(listener);
	}

	public void EventVizualizareStoc(ActionListener listener) {
		btnVizualizareStoc.addActionListener(listener);
	}

	public void EventStergereProdus(ActionListener listener) {
		btnStergereProdus.addActionListener(listener);
	}

	// sectiunea de preluare a textului din campurile interfetei grafice
	public String getNumeClient() {
		return numeClientText.getText();
	}

	public String getPrenumeClient() {
		return prenumeClientText.getText();
	}

	public String getTelefonClient() {
		return telefonClientText.getText();
	}

	public String getDataNasteriiClient() {
		return dataNasteriiClientText.getText();
	}

	public String getCreeareAbonament() {
		return creeareAbonamentText.getText();
	}

	public String getExpirareAbonament() {
		return expirareAbonamentText.getText();
	}

	public String getTipAbonament() {
		return tipAbonamentText.getText();
	}
	
	public String getPretAbonament() {
		return pretAbonamentText.getText();
	}

	public String getObservatiiClient() {
		return observatiiClientiText.getText();
	}

	public String getNumeClientNou() {
		return numeClientNou.getText();
	}

	public String getPrenumeClientNou() {
		return prenumeClientNou.getText();
	}

	public String getTelefonClientNou() {
		return telefonClientNou.getText();
	}

	public String getDataNasteriiClientNou() {
		return dataNasteriiClientNou.getText();
	}

	public String getCreeareAbonamentNou() {
		return creeareAbonamentNou.getText();
	}

	public String getExpirareAbonamentNou() {
		return expirareAbonamentNou.getText();
	}

	public String getTipAbonamentNou() {
		return tipAbonamentNou.getText();
	}
	
	public String getPretAbonamentNou() {
		return pretAbonamentNou.getText();
	}

	public String getObservatiiClientNou() {
		return observatiiClientiNou.getText();
	}

	public String getNumeProdusVanzare() {
		return numeProdusText.getText();
	}

	public String getCantitateVanzare() {
		return cantitateText.getText();
	}

	public String getNumeProdusStoc() {
		return numeProdusStocText.getText();
	}

	public String getCantitateStoc() {
		return cantitateStocText.getText();
	}

	public String getPretStoc() {
		return pretStocText.getText();
	}

	public String getDataExpirariiStoc() {
		return dataExpirariiStocText.getText();
	}
	

}
