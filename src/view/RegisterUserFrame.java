package view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JTextField;
import javax.swing.JButton;

public class RegisterUserFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JTextField numeAngajatText;
	private JTextField prenumeAngajatText;
	private JTextField telefonAngajatText;
	private JTextField dataNasteriiText;

	private JButton ok;
	
	public RegisterUserFrame() {
		setTitle("Inregistrare utilizator");
		setSize(700, 500);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel numeAngajat = new JLabel("Nume angajat");
		numeAngajat.setFont(new Font("Tahoma", Font.PLAIN, 30));
		numeAngajat.setBounds(30, 30, 250, 50);
		panel.add(numeAngajat);
		
		JLabel lblPrenumeAngajat = new JLabel("Prenume angajat");
		lblPrenumeAngajat.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPrenumeAngajat.setBounds(30, 100, 250, 50);
		panel.add(lblPrenumeAngajat);
		
		JLabel lblTelefonAngajat = new JLabel("Telefon angajat");
		lblTelefonAngajat.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTelefonAngajat.setBounds(30, 170, 250, 50);
		panel.add(lblTelefonAngajat);
		
		JLabel lblDataNasterii = new JLabel("Data nasterii");
		lblDataNasterii.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblDataNasterii.setBounds(30, 246, 250, 50);
		panel.add(lblDataNasterii);
		
		numeAngajatText = new JTextField();
		numeAngajatText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		numeAngajatText.setBounds(300, 40, 350, 40);
		panel.add(numeAngajatText);
		numeAngajatText.setColumns(10);
		
		prenumeAngajatText = new JTextField();
		prenumeAngajatText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		prenumeAngajatText.setColumns(10);
		prenumeAngajatText.setBounds(300, 110, 350, 40);
		panel.add(prenumeAngajatText);
		
		telefonAngajatText = new JTextField();
		telefonAngajatText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		telefonAngajatText.setColumns(10);
		telefonAngajatText.setBounds(300, 180, 350, 40);
		panel.add(telefonAngajatText);
		
		dataNasteriiText = new JTextField();
		dataNasteriiText.setFont(new Font("Tahoma", Font.PLAIN, 25));
		dataNasteriiText.setColumns(10);
		dataNasteriiText.setBounds(300, 250, 350, 40);
		panel.add(dataNasteriiText);
		
		ok = new JButton("OK");
		ok.setFont(new Font("Tahoma", Font.PLAIN, 25));
		ok.setBounds(275, 350, 150, 50);
		panel.add(ok);
	}
	
	public void EventOK(ActionListener listener) {
		ok.addActionListener(listener);
	}
	
	public String getNumeAngajat() {
		return numeAngajatText.getText();
	}
	
	public String getPrenumeAngajat() {
		return prenumeAngajatText.getText();
	}
	
	public String getTelefonAngajat() {
		return telefonAngajatText.getText();
	}
	
	public String getDataNasterii() {
		return dataNasteriiText.getText();
	}
}
