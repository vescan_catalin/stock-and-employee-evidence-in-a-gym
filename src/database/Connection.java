package database;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import model.*;

public class Connection {

	private Session session;

	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

	public Connection() {
		session = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		System.out.println("conexiune realizata");
	}

	public String concat(String a, String b, String c) {
		return a + b + c;
	}

	public List<?> getInformations(int type) throws Exception {

		if (type == 0)
			return session.createQuery("from Admin", Admin.class).list();
		if (type == 1)
			return session.createQuery("from User", User.class).list();
		if (type == 2)
			return session.createQuery("from InformatiiClienti", InformatiiClienti.class).list();
		if (type == 3)
			return session.createQuery("from InformatiiUtilizator", InformatiiUtilizator.class).list();
		if (type == 4)
			return session.createQuery("from Stoc", Stoc.class).list();

		// session.close();
		return null;
	}

	public void saveInformations(List<?> list) throws Exception {
		int type = (int) list.get(1);

		session.beginTransaction();

		switch (type) {
		case 1: // user account
			session.save((User) list.get(0));
			break;
		case 2: // client informations
			session.save((InformatiiClienti) list.get(0));
			break;
		case 3: // user informations
			session.save((InformatiiUtilizator) list.get(0));
			break;
		case 4: // stoc informations
			session.save((Stoc) list.get(0));
		}

		session.getTransaction().commit();

		// session.close();
	}

	public void delete(String pattern, int type) throws Exception {
		session.beginTransaction();

		String a = "'";

		if (type == 2) {// client
			session.createQuery("delete from InformatiiClienti where telefonClient = " + concat(a, pattern, a))
					.executeUpdate();
		}

		if (type == 3) { // user
			int userId = (int) session
					.createNativeQuery(
							"select userId from InformatiiUtilizator where telefonAngajat = " + concat(a, pattern, a))
					.uniqueResult();

			session.createQuery("delete from InformatiiUtilizator where telefonAngajat = " + concat(a, pattern, a))
					.executeUpdate();

			session.createQuery("delete from User where id = " + userId).executeUpdate();
		}

		if (type == 4) { // stoc
			session.createQuery("delete from Stoc where numeProdus = " + concat(a, pattern, a)).executeUpdate();
		}

		session.getTransaction().commit();
	}

	public void modifyInformations(List<?> list) throws Exception {
		int type = (int) list.get(2);

		session.beginTransaction();

		if (type == 1) { // userAccount

		}

		if (type == 2) { // client
			InformatiiClienti lastClient = (InformatiiClienti) list.get(1);
			InformatiiClienti newClient = (InformatiiClienti) list.get(0);

			String a = "'";

			String update = String.join(", ",
					"numeClient = " + (newClient.getNumeClient() == null
							? (lastClient.getNumeClient() == null ? null : concat(a, lastClient.getNumeClient(), a))
							: concat(a, newClient.getNumeClient(), a)),
					"prenumeClient = " + (newClient.getPrenumeClient() == null
							? (lastClient.getPrenumeClient() == null ? null
									: concat(a, lastClient.getPrenumeClient(), a))
							: concat(a, newClient.getPrenumeClient(), a)),
					"telefonClient = " + (newClient.getTelefonClient() == null
							? (lastClient.getTelefonClient() == null ? null
									: concat(a, lastClient.getTelefonClient(), a))
							: concat(a, newClient.getTelefonClient(), a)),
					"dataNasteriiClient = "
							+ (newClient.getDataNasteriiClient() == null
									? (lastClient.getDataNasteriiClient() == null ? null
											: concat("cast('", format.format(lastClient.getDataNasteriiClient()),
													"' as date)"))
									: concat("cast('", format.format(newClient.getDataNasteriiClient()), "' as date)")),
					"dataCreeareAbonament = "
							+ (newClient.getDataCreeareAbonament() == null
									? (lastClient.getDataCreeareAbonament() == null ? null
											: concat("cast('", format.format(lastClient.getDataCreeareAbonament()),
													"' as date)"))
									: concat("cast('", format.format(newClient.getDataCreeareAbonament()),
											"' as date)")),
					"dataExpirareAbonament = " + (newClient.getDataExpirareAbonament() == null
							? (lastClient.getDataExpirareAbonament() == null ? null
									: concat("cast('", format.format(lastClient.getDataExpirareAbonament()),
											"' as date)"))
							: concat("cast('", format.format(newClient.getDataExpirareAbonament()), "' as date)")),
					"tipAbonament = "
							+ (newClient.getTipAbonament() == null
									? (lastClient.getTipAbonament() == null
											? null
											: concat(a, lastClient.getTipAbonament(), a))
									: concat(a, newClient.getTipAbonament(), a)),
					"pret = " + (newClient.getPret() == 0 ? (lastClient.getPret() == 0 ? 0 : lastClient.getPret())
							: newClient.getPret()),
					"observatiiClient = " + (newClient.getObservatiiClient() == null
							? (lastClient.getObservatiiClient() == null ? null
									: concat(a, lastClient.getObservatiiClient(), a))
							: concat(a, newClient.getObservatiiClient(), a)));

			session.createQuery("update InformatiiClienti set " + update + " where telefonClient = "
					+ concat(a, lastClient.getTelefonClient(), a)).executeUpdate();
		}

		if (type == 3) { // user
			InformatiiUtilizator lastUser = (InformatiiUtilizator) list.get(1);
			InformatiiUtilizator newUser = (InformatiiUtilizator) list.get(0);

			String a = "'";

			String update = String.join(", ",
					"numeAngajat = " + (newUser.getNumeAngajat() == null
							? (lastUser.getNumeAngajat() == null ? null : concat(a, lastUser.getNumeAngajat(), a))
							: concat(a, newUser.getNumeAngajat(), a)),
					"prenumeAngajat = " + (newUser.getPrenumeAngajat() == null
							? (lastUser.getPrenumeAngajat() == null ? null : concat(a, lastUser.getPrenumeAngajat(), a))
							: concat(a, newUser.getPrenumeAngajat(), a)),
					"telefonAngajat = " + (newUser.getTelefonAngajat() == null
							? (lastUser.getTelefonAngajat() == null ? null : concat(a, lastUser.getTelefonAngajat(), a))
							: concat(a, newUser.getTelefonAngajat(), a)),
					"dataNasteriiAngajat = " + (newUser.getDataNasterii() == null
							? (format.format(lastUser.getDataNasterii()) == null ? null
									: concat("cast('", format.format(lastUser.getDataNasterii()), "' as date)"))
							: concat("cast('", format.format(newUser.getDataNasterii()), "' as date)")),
					"observatiiAngajat = " + (newUser.getObservatii() == null
							? (lastUser.getObservatii() == null ? null : concat(a, lastUser.getObservatii(), a))
							: concat(a, newUser.getObservatii(), a)));

			session.createQuery("update InformatiiUtilizator set " + update + " where telefonAngajat = "
					+ concat(a, lastUser.getTelefonAngajat(), a)).executeUpdate();
		}

		if (type == 4) { // stock
			String a = "'";

			Stoc newProduct = (Stoc) list.get(0);
			Stoc product = (Stoc) list.get(1);

			Pattern pattern = Pattern.compile("\\d+");
			Matcher matcher1 = pattern.matcher(product.getCantitate() == null ? "0" : product.getCantitate());
			Matcher matcher2 = pattern.matcher(newProduct.getCantitate() == null ? "0" : newProduct.getCantitate());

			int cantitateVeche = 0, cantitateNoua = 0;

			if (matcher1.find() && matcher2.find()) {
				cantitateVeche = Integer.parseInt(matcher1.group(0));
				cantitateNoua = Integer.parseInt(matcher2.group(0));
			}

			Pattern pattern2 = Pattern.compile("\\D+");
			Matcher matcher3 = pattern2.matcher(product.getCantitate() == null ? "" : product.getCantitate());
			Matcher matcher4 = pattern2.matcher(newProduct.getCantitate() == null ? "" : newProduct.getCantitate());
				
			String textCantitate = null;
			if (matcher3.find() && matcher4.find())
				textCantitate = matcher3.group(0) == null ? (matcher4.group(0) == null ? null : matcher4.group(0))
						: matcher3.group(0);

			matcher3 = pattern2.matcher(product.getCantitate() == null ? "" : product.getCantitate());
			matcher4 = pattern2.matcher(newProduct.getCantitate() == null ? "" : newProduct.getCantitate());
			
			if (!matcher3.find() && matcher4.find())
				textCantitate = matcher4.group(0) == null ? null : matcher4.group(0);

			matcher3 = pattern2.matcher(product.getCantitate() == null ? "" : product.getCantitate());
			matcher4 = pattern2.matcher(newProduct.getCantitate() == null ? "" : newProduct.getCantitate());
			
			if (matcher3.find() && !matcher4.find())
				textCantitate = matcher3.group(0) == null ? null : matcher3.group(0);

			String cantitate = null;
			if ((boolean) list.get(3)) {
				cantitate = "" + (cantitateVeche - cantitateNoua)
						+ (textCantitate == null
								? (matcher3.group(0) == null ? (matcher4.group(0) == null ? null : matcher4.group(0))
										: matcher3.group(0))
								: textCantitate);

				session.createQuery("update Stoc set cantitate = " + concat(a, cantitate, a) + " where numeProdus = "
						+ concat(a, product.getNumeProdus(), a)).executeUpdate();
			} else {
				cantitate = "" + (cantitateVeche + cantitateNoua)
						+ (textCantitate == null
								? (matcher3.group(0) == null ? (matcher4.group(0) == null ? null : matcher4.group(0))
										: matcher3.group(0))
								: textCantitate);

				String update = String.join(", ", "cantitate = " + concat(a, cantitate, a),
						"pret = " + (newProduct.getPret() == 0 ? product.getPret() : newProduct.getPret()));

				session.createQuery(
						"update Stoc set " + update + " where numeProdus = " + concat(a, product.getNumeProdus(), a))
						.executeUpdate();
			}
		}

		session.getTransaction().commit();
	}

}
