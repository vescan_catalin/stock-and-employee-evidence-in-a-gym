package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "Stoc", uniqueConstraints = { @UniqueConstraint(columnNames = { "id", "numeProdus"}) })
public class Stoc implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "numeProdus")
	private String numeProdus;
	
	@Column(name = "cantitate")
	private String cantitate;
	
	@Column(name = "pret")
	private float pret;
	
	@Column(name = "dataExpirarii")
	private Date dataExpirarii;
	
	public Stoc() {}
	
	public Stoc(String numeProdus, String cantitate, float pret, Date dataExpirarii) {
		this.numeProdus = numeProdus;
		this.cantitate = cantitate;
		this.pret = pret;
		this.dataExpirarii = dataExpirarii;
	}

	public String getNumeProdus() {
		return numeProdus;
	}

	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}

	public String getCantitate() {
		return cantitate;
	}

	public void setCantitate(String cantitate) {
		this.cantitate = cantitate;
	}

	public float getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public Date getDataExpirarii() {
		return dataExpirarii;
	}

	public void setDataExpirarii(Date dataExpirarii) {
		this.dataExpirarii = dataExpirarii;
	}

}
