package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "InformatiiClienti", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "id", "telefonClient" }) })
public class InformatiiClienti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "numeClient")
	private String numeClient;

	@Column(name = "prenumeClient")
	private String prenumeClient;

	@Column(name = "telefonClient")
	private String telefonClient;

	@Column(name = "dataNasteriiClient")
	private Date dataNasteriiClient;

	@Column(name = "dataCreeareAbonament")
	private Date dataCreeareAbonament;

	@Column(name = "dataExpirareAbonament")
	private Date dataExpirareAbonament;

	@Column(name = "tipAbonament")
	private String tipAbonament;
	
	@Column(name = "pret")
	private int pret;

	@Column(name = "observatiiClient")
	private String observatiiClient;

	public InformatiiClienti() {}
	
	public InformatiiClienti(String numeClient, String prenumeClient, String telefonClient, Date dataNasteriiClient,
			Date dataCreeareAbonament, Date dataExpirareAbonament, String tipAbonament, int pret, String observatiiClient) {
		this.numeClient = numeClient;
		this.prenumeClient = prenumeClient;
		this.telefonClient = telefonClient;
		this.dataNasteriiClient = dataNasteriiClient;
		this.dataCreeareAbonament = dataCreeareAbonament;
		this.dataExpirareAbonament = dataExpirareAbonament;
		this.tipAbonament = tipAbonament;
		this.pret = pret;
		this.observatiiClient = observatiiClient;
	}

	public String getNumeClient() {
		return numeClient;
	}

	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}

	public String getPrenumeClient() {
		return prenumeClient;
	}

	public void setPrenumeClient(String prenumeClient) {
		this.prenumeClient = prenumeClient;
	}

	public String getTelefonClient() {
		return telefonClient;
	}

	public void setTelefonClient(String telefonClient) {
		this.telefonClient = telefonClient;
	}

	public Date getDataNasteriiClient() {
		return dataNasteriiClient;
	}

	public void setDataNasteriiClient(Date dataNasteriiClient) {
		this.dataNasteriiClient = dataNasteriiClient;
	}

	public Date getDataCreeareAbonament() {
		return dataCreeareAbonament;
	}

	public void setDataCreeareAbonament(Date dataCreeareAbonament) {
		this.dataCreeareAbonament = dataCreeareAbonament;
	}

	public Date getDataExpirareAbonament() {
		return dataExpirareAbonament;
	}

	public void setDataExpirareAbonament(Date dataExpirareAbonament) {
		this.dataExpirareAbonament = dataExpirareAbonament;
	}

	public String getTipAbonament() {
		return tipAbonament;
	}

	public void setTipAbonament(String tipAbonament) {
		this.tipAbonament = tipAbonament;
	}

	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public String getObservatiiClient() {
		return observatiiClient;
	}

	public void setObservatiiClient(String observatiiClient) {
		this.observatiiClient = observatiiClient;
	}
}
