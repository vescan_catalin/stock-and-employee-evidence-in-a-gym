package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "InformatiiUtilizator", uniqueConstraints={@UniqueConstraint(columnNames = {"id" , "telefonAngajat"})})
public class InformatiiUtilizator implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "userId")
	private int userId;
	
	@Column(name = "numeAngajat")
	private String numeAngajat;
	
	@Column(name = "prenumeAngajat")
	private String prenumeAngajat;
	
	@Column(name = "telefonAngajat")
	private String telefonAngajat;
	
	@Column(name = "dataNasteriiAngajat")
	private Date dataNasterii;
	
	@Column(name = "observatiiAngajat")
	private String observatii;
	
	public InformatiiUtilizator() {}
	
	public InformatiiUtilizator(int userId, String numeAngajat, String prenumeAngajat, String telefonAngajat, Date dataNasterii, String observatii) {
		this.userId = userId;
		this.numeAngajat = numeAngajat;
		this.prenumeAngajat = prenumeAngajat;
		this.telefonAngajat = telefonAngajat;
		this.dataNasterii = dataNasterii;
		this.observatii = observatii;
	}
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getNumeAngajat() {
		return numeAngajat;
	}

	public void setNumeAngajat(String numeAngajat) {
		this.numeAngajat = numeAngajat;
	}

	public String getPrenumeAngajat() {
		return prenumeAngajat;
	}

	public void setPrenumeAngajat(String prenumeAngajat) {
		this.prenumeAngajat = prenumeAngajat;
	}

	public String getTelefonAngajat() {
		return telefonAngajat;
	}

	public void setTelefonAngajat(String telefonAngajat) {
		this.telefonAngajat = telefonAngajat;
	}

	public Date getDataNasterii() {
		return dataNasterii;
	}

	public void setDataNasterii(Date dataNasterii) {
		this.dataNasterii = dataNasterii;
	}

	public String getObservatii() {
		return observatii;
	}

	public void setObservatii(String observatii) {
		this.observatii = observatii;
	}

	
}
