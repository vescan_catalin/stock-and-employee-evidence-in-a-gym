package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JOptionPane;

import database.Connection;
import model.Admin;
import model.User;
import view.AdminFrame;
import view.Login;
import view.RegisterUserFrame;
import view.UserFrame;

public class LoginManagement {
	private final int admin = 0;
	private final int user = 1;

	public LoginManagement(Login login) {
		login.setVisible(true);

		login.registerEvent(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				boolean pass = true;

				if (login.getChckbxAdmin().isSelected() && login.getChckbxUser().isSelected()) {
					JOptionPane.showMessageDialog(null, "Alege doar o optiune", "Eroare", JOptionPane.ERROR_MESSAGE);
					pass = false;
				}

				if (login.getChckbxAdmin().isSelected() && pass)
					JOptionPane.showMessageDialog(null, "Exista deja un administrator", "Eroare",
							JOptionPane.ERROR_MESSAGE);

				if (login.getChckbxUser().isSelected() && pass) {
					String username = login.getUserText();
					String password = login.getPassText();

					List<User> users = null;
					try {
						users = (List<User>) new Connection().getInformations(user);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (users == null
							|| users.stream().filter(user -> user.getUsername().equals(username)).count() == 0)
						new RegisterUser(new RegisterUserFrame(), new Connection(), new User(username, password));
					else
						JOptionPane.showMessageDialog(null, "Exista deja un utilizator cu acest nume", "Eroare",
								JOptionPane.ERROR_MESSAGE);

				}
			}
		});

		login.loginEvent(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				boolean pass = true;

				if (login.getChckbxAdmin().isSelected() && login.getChckbxUser().isSelected()) {
					JOptionPane.showMessageDialog(null, "Alege doar o optiune", "Eroare", JOptionPane.ERROR_MESSAGE);
					pass = false;
				}

				if (login.getChckbxAdmin().isSelected() && pass) {
					String username = login.getUserText();
					String password = login.getPassText();

					List<Admin> admins = null;
					try {
						admins = (List<Admin>) new Connection().getInformations(admin);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (admins.stream().filter(
							admin -> admin.getUsername().equals(username) && admin.getPassword().equals(password))
							.count() != 0)
						new AdminController(new AdminFrame());
					else
						JOptionPane.showMessageDialog(null, "Acest administrator nu exista", "Eroare",
								JOptionPane.ERROR_MESSAGE);
				}

				if (login.getChckbxUser().isSelected() && pass) {
					String username = login.getUserText();
					String password = login.getPassText();

					List<User> users = null;
					try {
						users = (List<User>) new Connection().getInformations(user);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					if (users.stream()
							.filter(user -> user.getUsername().equals(username) && user.getPassword().equals(password))
							.count() != 0) {
						User user = users.stream()
								.filter(u -> u.getUsername().equals(username) && u.getPassword().equals(password))
								.findFirst().get();
						new UserController(new UserFrame(), user);
					} else
						JOptionPane.showMessageDialog(null, "Acest utilizator nu exista", "Eroare",
								JOptionPane.ERROR_MESSAGE);
				}
			}
		});
	}
}
