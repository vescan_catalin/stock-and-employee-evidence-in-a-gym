package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.JOptionPane;
import database.Connection;
import model.*;
import view.AdminFrame;
import view.ViewInformations;

public class AdminController {
	private final int userAccount = 1, clients = 2, users = 3, stoc = 4;

	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private List<Object> list = new ArrayList<Object>();
	private PrintWriter writer = null;
	private ViewInformations info = new ViewInformations();

	public AdminController(AdminFrame frame) {
		frame.setVisible(true);

		frame.EventCreeareAbonament(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String numeClient = frame.getNumeClient().equals("") ? null : frame.getNumeClient();
				String prenumeClient = frame.getPrenumeClient().equals("") ? null : frame.getPrenumeClient();
				String telefonClient = frame.getTelefonClient().equals("") ? null : frame.getTelefonClient();
				Date dataNasteriiClient = null, dataCreeareAbonament = null, dataExpirareAbonament = null;
				String tipAbonament = frame.getTipAbonament().equals("") ? null : frame.getTipAbonament();
				int pret = frame.getPretAbonament().equals("") ? 0 : Integer.parseInt(frame.getPretAbonament());
				String observatiiClient = frame.getObservatiiClient().equals("") ? null : frame.getObservatiiClient();

				List<InformatiiClienti> clienti = null;
				if (telefonClient != null) {
					try {
						dataNasteriiClient = frame.getDataNasteriiClient().equals("") ? null
								: format.parse(frame.getDataNasteriiClient());
						dataCreeareAbonament = frame.getCreeareAbonament().equals("") ? null
								: format.parse(frame.getCreeareAbonament());
						dataExpirareAbonament = frame.getExpirareAbonament().equals("") ? null
								: format.parse(frame.getExpirareAbonament());

						clienti = (List<InformatiiClienti>) new Connection().getInformations(clients);

						if (dataCreeareAbonament != null) {
							if (dataExpirareAbonament == null) {
								String[] d = format.format(dataCreeareAbonament).split("-");
								String day = "" + (Integer.parseInt(d[2]) - 1);
								String month = "" + (Integer.parseInt(d[1]) + 1);
								dataExpirareAbonament = format.parse(d[0] + "-" + month + "-" + day);
							}

							if (clienti == null || clienti.stream()
									.filter(client -> client.getTelefonClient().equals(telefonClient)).count() == 0) {
								list.clear();
								list.add(new InformatiiClienti(numeClient, prenumeClient, telefonClient,
										dataNasteriiClient, dataCreeareAbonament, dataExpirareAbonament, tipAbonament,
										pret, observatiiClient));
								list.add(clients);

								new Connection().saveInformations(list);

								String title = new SimpleDateFormat("dd.MM.yyyy")
										.format(Calendar.getInstance().getTime());
								String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss")
										.format(Calendar.getInstance().getTime());

								writer = new PrintWriter(new BufferedWriter(new FileWriter(
										"C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
								writer.println("ABONAMENT creat de catre administrator in " + data + "\t\t platit : "
										+ pret + " lei");
								writer.println("\n");
							} else
								JOptionPane.showMessageDialog(null, "Acest client exista deja in baza de date",
										"Eroare", JOptionPane.ERROR_MESSAGE);
						} else
							JOptionPane.showMessageDialog(null, "Data inceperii abonamentului este obligatorie!",
									"Eroare", JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						try {
							// Close the writer regardless of what happens...
							writer.close();
						} catch (Exception e) {
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "Telefonul este obligatoriu!", "Eroare",
							JOptionPane.ERROR_MESSAGE);
			}
		});

		frame.EventVizualizareDateClienti(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				info.setVisible(true);
				try {
					info.setTableText(new Connection().getInformations(clients), clients);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				info.okEvent(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						info.setVisible(false);
						info.dispose();
					}

				});
			}
		});

		frame.EventModificareDateClient(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String telefonClient = frame.getTelefonClient().equals("") ? null : frame.getTelefonClient();
				String nume = frame.getNumeClientNou().equals("") ? null : frame.getNumeClientNou();
				String prenume = frame.getPrenumeClientNou().equals("") ? null : frame.getPrenumeClientNou();
				String telefon = frame.getTelefonClientNou().equals("") ? null : frame.getTelefonClientNou();
				Date dataNasterii = null, dataCreeareAb = null, dataExpirareAb = null;
				String tipAbonament = frame.getTipAbonamentNou().equals("") ? null : frame.getTipAbonamentNou();
				int pret = frame.getPretAbonamentNou().equals("") ? 0 : Integer.parseInt(frame.getPretAbonamentNou());
				String observatii = frame.getObservatiiClientNou().equals("") ? null : frame.getObservatiiClientNou();
				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				List<InformatiiClienti> clienti = null;
				try {
					dataNasterii = frame.getDataNasteriiClientNou().equals("") ? null
							: format.parse(frame.getDataNasteriiClientNou());
					dataCreeareAb = frame.getCreeareAbonamentNou().equals("") ? null
							: format.parse(frame.getCreeareAbonamentNou());
					dataExpirareAb = frame.getExpirareAbonamentNou().equals("") ? null
							: format.parse(frame.getExpirareAbonamentNou());

					if (dataCreeareAb != null) {
						if (dataExpirareAb == null) {
							String[] d = format.format(dataCreeareAb).split("-");
							String day = "" + (Integer.parseInt(d[2]) - 1);
							String month = "" + (Integer.parseInt(d[1]) + 1);
							dataExpirareAb = format.parse(d[0] + "-" + month + "-" + day);
						}
						
						clienti = (List<InformatiiClienti>) new Connection().getInformations(clients);
						InformatiiClienti lastClient = clienti.stream()
								.filter(c -> c.getTelefonClient().equals(telefonClient)).findFirst().get();

						if (clienti.stream().filter(client -> client.getTelefonClient().equals(telefonClient))
								.count() != 0
								&& (clienti.stream().filter(client -> client.getTelefonClient().equals(telefon))
										.count() == 0 || telefonClient.equals(telefon) || telefon.equals(""))) {
							list.clear();
							list.add(new InformatiiClienti(nume, prenume, telefon, dataNasterii, dataCreeareAb,
									dataExpirareAb, tipAbonament, pret, observatii));
							list.add(lastClient);
							list.add(clients);

							new Connection().modifyInformations(list);

							writer = new PrintWriter(new BufferedWriter(
									new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
							writer.println("Administratorul a modificat datele clientului " + lastClient.getNumeClient()
									+ " " + lastClient.getPrenumeClient() + " in \t" + data);
							writer.println("\n");
						} else
							JOptionPane.showMessageDialog(null, "Acest client exista deja in baza de date", "Eroare",
									JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}
		});

		frame.EventStergereClient(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String telefonClient = frame.getTelefonClient();

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				List<InformatiiClienti> client = null;
				try {
					client = (List<InformatiiClienti>) new Connection().getInformations(clients);

					InformatiiClienti c = (InformatiiClienti) client.stream()
							.filter(x -> x.getTelefonClient().equals(telefonClient)).findFirst().get();

					String numeClient = c.getNumeClient();
					String prenumeClient = c.getPrenumeClient();

					new Connection().delete(telefonClient, clients);

					writer = new PrintWriter(new BufferedWriter(
							new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
					writer.println("Clientul " + numeClient + " " + prenumeClient
							+ " a fost sters din baza de date de catre administrator in \t" + data);
					writer.println("\n");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Acest client a fost sters sau nu exista in baza de date.",
							"Eroare", JOptionPane.ERROR_MESSAGE);
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}
		});

		frame.EventVerificareAbonamente(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String telefon = frame.getTelefonClient().equals("") ? "" : frame.getTelefonClient();

				List<InformatiiClienti> clienti = null;

				try {
					clienti = (List<InformatiiClienti>) new Connection().getInformations(clients);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				if (telefon.equals("")) {
					list.clear();
					list = clienti.stream()
							.filter(client -> client.getDataExpirareAbonament().compareTo(new Date()) <= 0)
							.sorted((c1, c2) -> c1.getDataExpirareAbonament().compareTo(c2.getDataExpirareAbonament()))
							.collect(Collectors.toList());
					info.setTableText(list, clients);
				} else {
					list.clear();
					list = clienti.stream()
							.filter(client -> client.getTelefonClient().equals(telefon)
									&& client.getDataExpirareAbonament().compareTo(new Date()) <= 0)
							.collect(Collectors.toList());
					info.setTableText(list, clients);
				}

				info.setVisible(true);

				info.okEvent(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						info.setVisible(false);
						info.dispose();
					}

				});

			}

		});

		frame.EventAdaugareAngajat(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String numeUtilizator = frame.getNumeUtilizator().equals("") ? null : frame.getNumeUtilizator();
				String parolaUtilizator = frame.getParolaUtilizator().equals("") ? null : frame.getParolaUtilizator();
				String numeAngajat = frame.getNumeAngajat().equals("") ? null : frame.getNumeAngajat();
				String prenumeAngajat = frame.getPrenumeAngajat().equals("") ? null : frame.getPrenumeAngajat();
				String telefonAngajat = frame.getTelefonAngajat().equals("") ? null : frame.getTelefonAngajat();
				Date dataNasterii = null;
				String observatii = frame.getObservatiiAngajat().equals("") ? null : frame.getObservatiiAngajat();

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				List<InformatiiUtilizator> user = null;
				List<User> userAcc = null;
				try {
					dataNasterii = frame.getDataNasteriiAngajat().equals("") ? null
							: format.parse(frame.getDataNasteriiAngajat());

					userAcc = (List<User>) new Connection().getInformations(userAccount);

					if (numeUtilizator != null || parolaUtilizator != null) {

						if (userAcc == null || userAcc.stream().filter(
								u -> u.getUsername().equals(numeUtilizator) && u.getPassword().equals(parolaUtilizator))
								.count() == 0) {
							list.clear();
							list.add(new User(numeUtilizator, parolaUtilizator));
							list.add(userAccount);

							new Connection().saveInformations(list);
						}

						user = (List<InformatiiUtilizator>) new Connection().getInformations(users);

						if (user == null || user.stream().filter(u -> u.getTelefonAngajat().equals(telefonAngajat))
								.count() == 0) {

							userAcc = (List<User>) new Connection().getInformations(userAccount);

							int userId = userAcc.stream().filter(u -> u.getUsername().equals(numeUtilizator)
									&& u.getPassword().equals(parolaUtilizator)).findFirst().get().getId();

							list.clear();
							list.add(new InformatiiUtilizator(userId, numeAngajat, prenumeAngajat, telefonAngajat,
									dataNasterii, observatii));
							list.add(users);

							new Connection().saveInformations(list);
						}

						writer = new PrintWriter(new BufferedWriter(
								new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
						writer.println("Administratorul a creat un nou cont pentru angajat cu urmatoarele date :");
						writer.println("Nume utilizator : " + numeUtilizator);
						writer.println("Parola utilizator : " + parolaUtilizator);
						writer.println("si informatiile corespunzatoare acestuia in \t" + data);
						writer.println("\n");
					} else
						JOptionPane.showMessageDialog(null, "Numele si parola utilizatorului sunt obligatorii!",
								"Eroare", JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}

		});

		frame.EventVizualizareDateAngajat(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				info.setVisible(true);
				try {
					info.setTableText(new Connection().getInformations(users), users);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				info.okEvent(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						info.setVisible(false);
						info.dispose();
					}

				});
			}
		});

		frame.EventModificareDateAngajat(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String telefonAngajat = frame.getTelefonAngajat().equals("") ? null : frame.getTelefonAngajat();
				String nume = frame.getNumeAngajatNou().equals("") ? null : frame.getNumeAngajatNou();
				String prenume = frame.getPrenumeAngajatNou().equals("") ? null : frame.getPrenumeAngajatNou();
				String telefon = frame.getTelefonAngajatNou().equals("") ? null : frame.getTelefonAngajatNou();
				Date dataNasterii = null;
				String observatii = frame.getObservatiiAngajatNou().equals("") ? null : frame.getObservatiiAngajatNou();

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				List<InformatiiUtilizator> user = null;
				try {
					dataNasterii = frame.getDataNasteriiAngajatNou().equals("") ? null
							: format.parse(frame.getDataNasteriiAngajatNou());

					user = (List<InformatiiUtilizator>) new Connection().getInformations(users);

					InformatiiUtilizator lastUser = null;
					if (user.stream().filter(c -> c.getTelefonAngajat().equals(telefonAngajat)).count() != 0) {
						lastUser = user.stream().filter(c -> c.getTelefonAngajat().equals(telefonAngajat)).findFirst()
								.get();

						if (user.stream().filter(u -> u.getTelefonAngajat().equals(telefonAngajat)).count() != 0
								&& (user.stream().filter(client -> client.getTelefonAngajat().equals(telefon))
										.count() == 0 || telefonAngajat.equals(telefon) || telefon.equals(""))) {
							list.clear();
							list.add(new InformatiiUtilizator(lastUser.getUserId(), nume, prenume, telefon,
									dataNasterii, observatii));
							list.add(lastUser);
							list.add(users);

							new Connection().modifyInformations(list);

							writer = new PrintWriter(new BufferedWriter(
									new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
							writer.println("Administratorul a modificat datele angajatului " + lastUser.getNumeAngajat()
									+ " " + lastUser.getPrenumeAngajat() + " in \t" + data);
							writer.println("\n");
						}
					} else
						JOptionPane.showMessageDialog(null, "Acest client exista deja in baza de date", "Eroare",
								JOptionPane.ERROR_MESSAGE);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}

		});

		frame.EventStergereAngajat(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String telefonAngajat = frame.getTelefonAngajat().equals("") ? null : frame.getTelefonAngajat();

				if (telefonAngajat != null) {
					List<InformatiiUtilizator> user = null;
					try {
						user = (List<InformatiiUtilizator>) new Connection().getInformations(users);

						InformatiiUtilizator c = null;
						String numeAngajat = null, prenumeAngajat = null;
						if (user.stream().filter(u -> u.getTelefonAngajat().equals(telefonAngajat)).count() != 0) {
							c = (InformatiiUtilizator) user.stream()
									.filter(x -> x.getTelefonAngajat().equals(telefonAngajat)).findFirst().get();
							numeAngajat = c.getNumeAngajat();
							prenumeAngajat = c.getPrenumeAngajat();

							new Connection().delete(telefonAngajat, users);

							String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
							String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss")
									.format(Calendar.getInstance().getTime());

							writer = new PrintWriter(new BufferedWriter(
									new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
							writer.println("Angajatul " + numeAngajat + " " + prenumeAngajat
									+ " a fost sters din baza de date de catre administrator in \t" + data);
							writer.println("\n");
						} else
							JOptionPane.showMessageDialog(null,
									"Acest angajat a fost sters sau nu exista in baza de date.", "Eroare",
									JOptionPane.ERROR_MESSAGE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} finally {
						try {
							// Close the writer regardless of what happens...
							writer.close();
						} catch (Exception e) {
						}
					}
				} else
					JOptionPane.showMessageDialog(null, "Acest angajat a fost sters sau nu exista in baza de date.",
							"Eroare", JOptionPane.ERROR_MESSAGE);

			}
		});

		frame.EventVanzare(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String numeProdus = frame.getNumeProdusVanzare().equals("") ? null : frame.getNumeProdusVanzare();
				String cantitate = frame.getCantitateVanzare().equals("") ? null : frame.getCantitateVanzare();

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				try {
					List<Stoc> stock = (List<Stoc>) new Connection().getInformations(stoc);

					writer = new PrintWriter(new BufferedWriter(
							new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));

					if (numeProdus == null || cantitate == null)
						JOptionPane.showMessageDialog(null, "Completeaza toate campurile!", "Eroare",
								JOptionPane.ERROR_MESSAGE);
					else {
						if (stock.stream().filter(stoc -> stoc.getNumeProdus().equals(numeProdus)).count() > 0) {
							Stoc product = stock.stream().filter(s -> s.getNumeProdus().equals(numeProdus)).findFirst()
									.get();
							list.clear();
							list.add(new Stoc(numeProdus, cantitate, 0, null));
							list.add(product);
							list.add(stoc);
							list.add(true);

							Pattern pattern = Pattern.compile("\\d+");
							Matcher matcher = pattern.matcher(cantitate);

							Pattern pattern2 = Pattern.compile("\\D+");
							Matcher matcher2 = pattern2.matcher(product.getCantitate());

							new Connection().modifyInformations(list);

							int vanzare = 0;
							String textCantitate = null;
							if (matcher.find() && matcher2.find()) {
								vanzare = Integer.parseInt(matcher.group(0));
								textCantitate = matcher.group(0) + matcher2.group(0);
							}

							writer.println("Administratorul a vandut " + textCantitate + " de " + numeProdus
									+ "\t pret platit : " + (vanzare * product.getPret()) + " lei in \t" + data);
							writer.println("\n");
						} else
							JOptionPane.showMessageDialog(null, "Acest produs nu este disponibil in stoc", "Eroare",
									JOptionPane.ERROR_MESSAGE);
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}
		});

		frame.EventAdaugareProdus(new ActionListener() {

			@SuppressWarnings("unchecked")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String numeProdus = frame.getNumeProdusStoc().equals("") ? null : frame.getNumeProdusStoc();
				String cantitate = frame.getCantitateStoc().equals("") ? null : frame.getCantitateStoc();
				float pret = frame.getPretStoc().equals("") ? 0 : Float.parseFloat(frame.getPretStoc());
				Date dataExpirarii = null;

				List<Stoc> stock = null;

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				try {
					dataExpirarii = frame.getDataExpirariiStoc().equals("") ? null
							: format.parse(frame.getDataExpirariiStoc());

					stock = (List<Stoc>) new Connection().getInformations(stoc);

					writer = new PrintWriter(new BufferedWriter(
							new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));

					if (numeProdus == null)
						JOptionPane.showMessageDialog(null, "Numele produsului e obligatoriu!", "Eroare",
								JOptionPane.ERROR_MESSAGE);
					else {
						if (stock.stream().filter(s -> s.getNumeProdus().equals(numeProdus)).count() > 0) {
							Stoc product = stock.stream().filter(s -> s.getNumeProdus().equals(numeProdus)).findFirst()
									.get();

							list.clear();
							list.add(new Stoc(numeProdus, cantitate, pret, dataExpirarii));
							list.add(product);
							list.add(stoc);
							list.add(false);

							new Connection().modifyInformations(list);

							Pattern pattern = Pattern.compile("\\d+");
							Matcher matcher = pattern.matcher(cantitate);

							Pattern pattern2 = Pattern.compile("\\D+");
							Matcher matcher2 = pattern2.matcher(product.getCantitate());

							String textCantitate = null;
							if (matcher.find() && matcher2.find())
								textCantitate = matcher.group(0) + matcher2.group(0);

							writer.println("Administratorul a adaugat " + textCantitate + " de " + numeProdus
									+ " in stoc in " + data);
							writer.println("\n");
						} else {
							list.clear();
							list.add(new Stoc(numeProdus, cantitate, pret, dataExpirarii));
							list.add(stoc);
							list.add(false);

							new Connection().saveInformations(list);

							writer.println("Administratorul a adaugat :");
							writer.println("nume produs : " + numeProdus);
							writer.println("cantitate : " + cantitate);
							writer.println("pret vanzare : " + pret + " lei");
							writer.println("in stoc in \t" + data);
							writer.println("\n");
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					try {
						// Close the writer regardless of what happens...
						writer.close();
					} catch (Exception e) {
					}
				}
			}
		});

		frame.EventVizualizareStoc(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				try {
					info.setTableText(new Connection().getInformations(stoc), stoc);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				info.setVisible(true);
				info.okEvent(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						info.setVisible(false);
						info.dispose();
					}

				});
			}
		});

		frame.EventStergereProdus(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String numeProdus = frame.getNumeProdusStoc().equals("") ? null : frame.getNumeProdusStoc();

				String title = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
				String data = new SimpleDateFormat("dd.MM.yyyy - hh:mm:ss").format(Calendar.getInstance().getTime());

				if (numeProdus == null)
					JOptionPane.showMessageDialog(null, "Acest produs nu exista in baza de date!", "Eroare",
							JOptionPane.ERROR_MESSAGE);
				else {
					try {
						new Connection().delete(numeProdus, stoc);

						writer = new PrintWriter(new BufferedWriter(
								new FileWriter("C:/Users/VESCAN/Desktop/aplicatie test/" + title + ".txt", true)));
						writer.println("Administratorul a scos produsul : " + numeProdus + " din stoc in \t" + data);
						writer.println("\n");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} finally {
						try {
							// Close the writer regardless of what happens...
							writer.close();
						} catch (Exception e1) {
						}
					}
				}
			}

		});

	}
}
