package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.JOptionPane;
import database.Connection;
import model.InformatiiUtilizator;
import model.User;
import view.RegisterUserFrame;

public class RegisterUser {
	private final int user = 1;
	private final int userInformations = 3;
	
	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
	
	private List<Object> list = new ArrayList<Object>();
	
	public RegisterUser(RegisterUserFrame frame, Connection connection, User userAccount) {
		frame.setVisible(true);
		
		frame.EventOK(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String numeAngajat = frame.getNumeAngajat();
				String prenumeAngajat = frame.getPrenumeAngajat();
				String telefonAngajat = frame.getTelefonAngajat();
				Date dataNasterii = null;
				
				try {
					dataNasterii = frame.getDataNasterii().equals("") ? null : format.parse(frame.getDataNasterii());
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				list.add(userAccount);
				list.add(user);
				
				try {
					connection.saveInformations(list);
					
					list.clear();
					list.add(new InformatiiUtilizator(userAccount.getId(), numeAngajat, prenumeAngajat, telefonAngajat, dataNasterii, null));
					list.add(userInformations);
					
					connection.saveInformations(list);
					
					JOptionPane.showMessageDialog(null, "Inregistrare efectuata cu succes.", "", JOptionPane.INFORMATION_MESSAGE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
				frame.setVisible(false);
			}
			
		});
		
	}
	
}
